package fr.natsystem.eval.datamodel;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="COEFF_CONTEXTE_PC")
public class CoefficientContextePC implements Serializable{


	private static final long serialVersionUID = 1080432852446594116L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="COEFF_CONTEXTE_PC_ID")
	private int id;
	
	@Column(name="COEFF_CONTEXTE_PC_CODE")
	private String code;
	
	@Column(name="COEFF_CONTEXTE_PC_VALEUR")
	private double valeur;
	
	@Column(name="COEFF_CONTEXTE_PC_LIBELLE")
	private String libelle;

	public CoefficientContextePC() {
		super();
	}

	public CoefficientContextePC(String code, double valeur, String libelle) {
		super();
		this.code = code;
		this.valeur = valeur;
		this.libelle = libelle;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public double getValeur() {
		return valeur;
	}

	public void setValeur(double valeur) {
		this.valeur = valeur;
	}

	public String getLibelle() {
		return libelle;
	}

	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}

	public int getId() {
		return id;
	}
	
}
