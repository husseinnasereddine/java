package fr.natsystem.eval.test;

import javax.inject.Inject;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import fr.natsystem.eval.datamodel.CoefficientContexteAgent;
import fr.natsystem.eval.datamodel.CoefficientContextePC;
import fr.natsystem.eval.datamodel.Gravite;
import fr.natsystem.eval.datamodel.SAMI;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "/applicationContext.xml" })
public class TestCreationParametres {

	@Inject
	SessionFactory factory;

	@Test
	public void testCreationSAMI() {
		SAMI TF = new SAMI("TF", 0, "Très faible", 2, "de 0% à 2%");
		SAMI F = new SAMI("F", 4, "Faible", 10, "de 2% à 10%");
		SAMI E = new SAMI("E", 20, "Elevée", 20, "de 10% à 20%");
		SAMI TE = new SAMI("TE", 100, "Très élevée", 100, "de 20% à 100%");
		SAMI NC = new SAMI("NC", 0, "Non concerné", 0, "");
		SAMI SD = new SAMI("SD", 40, "Sans données", 0, "");

		TF.setColor("146,208,80");
		F.setColor("0,176,240");
		E.setColor("255,192,0");
		TE.setColor("255,0,0");
		NC.setColor("217,217,217");
		SD.setColor("255,192,0");
		Session session = this.factory.openSession();
		Transaction tx = session.beginTransaction();

		session.saveOrUpdate(TF);
		session.saveOrUpdate(F);
		session.saveOrUpdate(E);
		session.saveOrUpdate(TE);
		session.saveOrUpdate(NC);
		session.saveOrUpdate(SD);

		tx.commit();
	}

	@Test
	public void testCreationCoeffContextePC() {
		CoefficientContextePC FA = new CoefficientContextePC("FA", 0.8, "Favorable");
		CoefficientContextePC DE = new CoefficientContextePC("DE", 1.2, "Défavorable");
		CoefficientContextePC N = new CoefficientContextePC("N", 1, "Neutre");

		Session session = this.factory.openSession();
		Transaction tx = session.beginTransaction();

		session.saveOrUpdate(FA);
		session.saveOrUpdate(DE);
		session.saveOrUpdate(N);

		tx.commit();
	}

	@Test
	public void testCreationCoeffContexteAgent() {
		CoefficientContexteAgent FA = new CoefficientContexteAgent("FA", 0.8, "Favorable");
		CoefficientContexteAgent DE = new CoefficientContexteAgent("DE", 1.2, "Défavorable");
		CoefficientContexteAgent N = new CoefficientContexteAgent("N", 1, "Neutre");
		Session session = this.factory.openSession();
		Transaction tx = session.beginTransaction();

		session.saveOrUpdate(FA);
		session.saveOrUpdate(DE);
		session.saveOrUpdate(N);

		tx.commit();
	}

	@Test
	public void testCreationGravite() {
		Gravite gr1 = new Gravite(1, "Peu important");
		Gravite gr2 = new Gravite(2, "Moyennement important");
		Gravite gr3 = new Gravite(3, "Important");
		Gravite gr4 = new Gravite(4, "Très important");
		Gravite gr5 = new Gravite(5, "Extrêmement important");
		Gravite gr6 = new Gravite(6, "Critique");

		Session session = this.factory.openSession();
		Transaction tx = session.beginTransaction();

		session.saveOrUpdate(gr1);
		session.saveOrUpdate(gr2);
		session.saveOrUpdate(gr3);
		session.saveOrUpdate(gr4);
		session.saveOrUpdate(gr5);
		session.saveOrUpdate(gr6);

		tx.commit();

	}
}
