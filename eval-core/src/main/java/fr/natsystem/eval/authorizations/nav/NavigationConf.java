package fr.natsystem.eval.authorizations.nav;

import java.util.ArrayList;
import java.util.List;

/**
 * Configuration object for the automatic construction of a BulletList in the LeftNavigationPanel
 *
 * @author NatSystem
 */

public class NavigationConf {

    List<NavigationGroup> navigationParts;

    public NavigationConf() {
        this.navigationParts = new ArrayList<NavigationGroup>();

    }

    public List<NavigationGroup> getNavigationGroups() {
        return navigationParts;
    }

    public void addNavGroup(NavigationGroup group) {
        this.navigationParts.add(group);

    }

    public void setGroups(List<NavigationGroup> groups){
        this.navigationParts = groups;
    }


}
