package fr.natsystem.eval.datamodel;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="THEME")
public class Theme implements Serializable{

	
	private static final long serialVersionUID = -501167048438057553L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="THEME_ID")
	private int id;
	
	@Column(name="THEME_LIBELLE")
	private String libelle;
	
	@ManyToOne
	@JoinColumn(name="THEME_DOMAINE_REF")
	private Domaine domaineRef;

	public Theme() {
	}

	public Theme(String libelle) {
		this.libelle = libelle;
	}
	public Theme(String libelle, Domaine domaine) {
		this.domaineRef = domaine;
		this.libelle = libelle;
	}
	public int getId() {
		return id;
	}

	public void setId(int idTheme) {
		this.id = idTheme;
	}

	public String getLibelle() {
		return libelle;
	}

	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}

	public Domaine getDomaineRef() {
		return domaineRef;
	}

	public void setDomaineRef(Domaine domaineRef) {
		this.domaineRef = domaineRef;
	}
}
