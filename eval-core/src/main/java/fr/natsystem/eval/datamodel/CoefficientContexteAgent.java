package fr.natsystem.eval.datamodel;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name="COEFF_CONTEXTE_AGENT")
public class CoefficientContexteAgent implements Serializable{
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="COEFF_CONTEXTE_AGENT_ID")
	private int id;

	@Column(name="COEFF_CONTEXTE_AGENT_CODE")
	private String code;
	
	@Column(name="COEFF_CONTEXTE_AGENT_VALEUR")
	private double valeur;
	
	@Column(name="COEFF_CONTEXTE_AGENT_LIBELLE")
	private String libelle;

	public CoefficientContexteAgent() {
		super();
	}

	public CoefficientContexteAgent(String code, double valeur, String libelle) {
		super();
		this.code = code;
		this.valeur = valeur;
		this.libelle = libelle;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public double getValeur() {
		return valeur;
	}

	public void setValeur(double valeur) {
		this.valeur = valeur;
	}

	public String getLibelle() {
		return libelle;
	}

	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}

	public int getId() {
		return id;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((code == null) ? 0 : code.hashCode());
		result = prime * result + id;
		result = prime * result + ((libelle == null) ? 0 : libelle.hashCode());
		long temp;
		temp = Double.doubleToLongBits(valeur);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CoefficientContexteAgent other = (CoefficientContexteAgent) obj;
		if (code == null) {
			if (other.code != null)
				return false;
		} else if (!code.equals(other.code))
			return false;
		if (id != other.id)
			return false;
		if (libelle == null) {
			if (other.libelle != null)
				return false;
		} else if (!libelle.equals(other.libelle))
			return false;
		if (Double.doubleToLongBits(valeur) != Double
				.doubleToLongBits(other.valeur))
			return false;
		return true;
	}

	
}
