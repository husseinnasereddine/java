package fr.natsystem.eval.dao.impl;

import java.util.List;

import org.springframework.stereotype.Component;

import fr.natsystem.eval.dao.IPointsClesDAO;
import fr.natsystem.eval.datamodel.Domaine;
import fr.natsystem.eval.datamodel.Espace;
import fr.natsystem.eval.datamodel.PointCle;
import fr.natsystem.eval.datamodel.Theme;

@Component
public class PointCleDAO implements IPointsClesDAO {

	/* (non-Javadoc)
	 * @see fr.natsystem.eval.dao.IPointsClesDAO#getAllPointsCles()
	 */
	@Override
	public List<PointCle> getAllPointsCles() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see fr.natsystem.eval.dao.IPointsClesDAO#getAllPointsClesOrderedByTheme()
	 */
	@Override
	public List<PointCle> getAllPointsClesOrderedByTheme() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see fr.natsystem.eval.dao.IPointsClesDAO#getPointsClesParTheme(fr.natsystem.eval.datamodel.Theme)
	 */
	@Override
	public List<PointCle> getPointsClesParTheme(Theme theme) {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see fr.natsystem.eval.dao.IPointsClesDAO#getPointsClesParDomaine(fr.natsystem.eval.datamodel.Domaine)
	 */
	@Override
	public List<PointCle> getPointsClesParDomaine(Domaine domaine) {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see fr.natsystem.eval.dao.IPointsClesDAO#getPointsClesParEspace(fr.natsystem.eval.datamodel.Espace)
	 */
	@Override
	public List<PointCle> getPointsClesParEspace(Espace espace) {
		// TODO Auto-generated method stub
		return null;
	}

//	@Override
//	public List<PointCle> getAllPointsCles() {
//		WhereClauseBuilder wcb = this.buildQueryForSearchTM(new PointCle(), null);
//		wcb.setOrderBy("ordre");
//		NatOrbQuery query = wcb.getQuery();
//		List<PointCle> list = (List<PointCle>) query.execute();
//		return list;
//	}
//	
//	@Override
//	public List<PointCle> getAllPointsClesOrderedByTheme() {
//		WhereClauseBuilder wcb = this.buildQueryForSearchTM(new PointCle(), null);
//		wcb.setOrderBy("themeRef.libelle");
//		NatOrbQuery query = wcb.getQuery();
//		List<PointCle> list = (List<PointCle>) query.execute();
//		return list;
//	}
//
//	@Override
//	public List<PointCle> getPointsClesParTheme(Theme themeRef) {
//		WhereClauseBuilder wcb = this.buildQueryForSearchTM(new PointCle(null, themeRef, null, 0), null);
//		wcb.setOrderBy("ordre");
//		wcb.setPropertyFromManyToOne("themeRef", "id", WhereClauseBuilder.EQUAL);
//		NatOrbQuery query = wcb.getQuery();
//		List<PointCle> list = (List<PointCle>) query.execute();
//		return list;
//	}
//
//	@Override
//	public List<PointCle> getPointsClesParDomaine(Domaine domaine) {
//		Theme theme = new Theme(null, domaine);
//		WhereClauseBuilder wcb = this.buildQueryForSearchTM(new PointCle(null, theme, null, 0), null);
//		wcb.setOrderBy("ordre");
//		NatOrbQuery query = wcb.getQuery();
//		List<PointCle> list = (List<PointCle>) query.execute();
//		return list;
//	}
//
//	@Override
//	public List<PointCle> getPointsClesParEspace(Espace espace) {
//		Domaine domaine = new Domaine(null, espace);
//		Theme theme = new Theme(null, domaine);
//		WhereClauseBuilder wcb = this.buildQueryForSearchTM(new PointCle(null, theme, null, 0), null);
//		wcb.setOrderBy("ordre");
//		NatOrbQuery query = wcb.getQuery();
//		List<PointCle> list = (List<PointCle>) query.execute();
//		return list;
//	}
//
//
//	@Override
//	public void deletePrimitiveMs(PointCle entity,
//			fr.natsystem.modele.service.AbstractNatorbDao.BeanTypeEnum pType)
//			throws BusinessLogicException {
//		NatOrbSession session = NatOrbUtil.getSession();
//		NatTransaction transaction = session.beginTransaction();
//		session.delete(entity);
//		transaction.commit();
//		NatOrbUtil.closeSession();
//	}

}
