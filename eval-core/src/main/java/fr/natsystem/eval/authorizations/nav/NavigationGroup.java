package fr.natsystem.eval.authorizations.nav;

import java.util.List;


/**
 * Class allowing to hold the definition of a navigation group. For instance in a LeftNavigationPanel. 
 * @author NatSystem

 */
public class NavigationGroup {
	
	private NavigationPart definition;
	
	private List<NavigationAction> actions;

	/**
	 * Returns the ui info of the group, the label and the eventual icon.
	 * @return
	 */
	public NavigationPart getDefinition() {
		return definition;
	}

	public void setDefinition(NavigationPart definition) {
		this.definition = definition;
	}

	public List<NavigationAction> getActions() {
		return actions;
	}
	
	public void setActions(List<NavigationAction> actions) {
		this.actions = actions;
	}

	
	

}
