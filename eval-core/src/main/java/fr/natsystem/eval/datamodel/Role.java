package fr.natsystem.eval.datamodel;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import fr.natsystem.eval.authorizations.IRole;



@Entity
@Table(name="ROLE")
public class Role implements IRole, Serializable{
	
	

	private static final long serialVersionUID = -8631070977647410191L;


	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="ROLE_ID")
	private int id;
	
	
	@Column(name="ROLE_LIBELLE")
	private String libelle;
	
	
	public Role() {
	}
	
	public Role(String libelle) {
		this.libelle = libelle;
	}

	@Override
	public String getLibelle() {
		return this.libelle;
	}

	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}
	
	public static String getFormattedRoles(List<Role> listeRoles) {
		String formattedRoles = "";
		for (Role role : listeRoles) {
			if (!formattedRoles.contains(role.getLibelle())) {
				if ("".equals(formattedRoles)) {
					formattedRoles += role.getLibelle();
				} else {
					formattedRoles += ", " + role.getLibelle();
				}
			}
		}
		return formattedRoles;
	}

}
