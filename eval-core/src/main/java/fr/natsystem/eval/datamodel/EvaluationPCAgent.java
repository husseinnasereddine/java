package fr.natsystem.eval.datamodel;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="EVALUATION_UNITAIRE")
public class EvaluationPCAgent implements Serializable{


	private static final long serialVersionUID = 12411976633989420L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="EVALUATION_UNITAIRE_ID")
	private int id;
	
	@ManyToOne
	@JoinColumn(name="EVALUATION_UNITAIRE_PC_REF")
	private PointCle pointcleRef;
	
	@ManyToOne
	@JoinColumn(name="EVALUATION_UNITAIRE_AGENT_REF")
	private Agent agentRef;
	
	@ManyToOne
	@JoinColumn(name="EVALUATION_UNITAIRE_CYCLE")
	private Cycle cycleRef;
	
	@ManyToOne
	@JoinColumn(name="EVALUATION_UNITAIRE_SAMI")
	private SAMI sami;
	
	
	@Column(name="EVALUATION_UNITAIRE_ACTIF")
	private boolean flagActif;
	
	@Column(name="EVALUATION_UNITAIRE_ETAT")
	private String etat;
	
	@ManyToOne
	@JoinColumn(name="EVALUATION_UNITAIRE_SAISISSEUR")
	private Agent dpxSaisisseur;

	public EvaluationPCAgent() {
		super();
	}
	
	

	public EvaluationPCAgent(PointCle pointcleRef, Agent agentRef,
			Cycle cycleRef, SAMI sami, boolean actif, String etat, Agent dpxSaisisseur) {
		super();
		this.pointcleRef = pointcleRef;
		this.agentRef = agentRef;
		this.cycleRef = cycleRef;
		this.sami = sami;
		this.flagActif = actif;
		this.etat = etat;
		this.dpxSaisisseur = dpxSaisisseur;
	}



	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public PointCle getPointcleRef() {
		return pointcleRef;
	}

	public void setPointcleRef(PointCle pointcleRef) {
		this.pointcleRef = pointcleRef;
	}

	public Agent getAgentRef() {
		return agentRef;
	}

	public void setAgentRef(Agent agentRef) {
		this.agentRef = agentRef;
	}
	
	public Cycle getCycleRef() {
		return cycleRef;
	}

	public void setCycleRef(Cycle cycleRef) {
		this.cycleRef = cycleRef;
	}

	public SAMI getSami() {
		return sami;
	}

	public void setSami(SAMI sami) {
		this.sami = sami;
	}



	public boolean isFlagActif() {
		return flagActif;
	}



	public void setFlagActif(boolean flagActif) {
		this.flagActif = flagActif;
	}



	public String getEtat() {
		return etat;
	}



	public void setEtat(String etat) {
		this.etat = etat;
	}



	public Agent getDpxSaisisseur() {
		return dpxSaisisseur;
	}



	public void setDpxSaisisseur(Agent dpxSaisisseur) {
		this.dpxSaisisseur = dpxSaisisseur;
	}



	@Override
	public String toString() {
		return "EvaluationPCAgent [id=" + id + ", pointcleRef=" + pointcleRef
				+ ", agentRef=" + agentRef + ", cycleRef=" + cycleRef
				+ ", sami=" + sami + ", flagActif=" + flagActif + ", etat="
				+ etat + ", dpxSaisisseur=" + dpxSaisisseur + "]";
	}



	
	
	
	

}
