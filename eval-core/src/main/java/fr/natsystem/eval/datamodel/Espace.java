package fr.natsystem.eval.datamodel;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="ESPACE")
public class Espace implements Serializable{


	private static final long serialVersionUID = -3452931133767456169L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="ESPACE_ID")
	private int id;
	
	@Column(name="ESPACE_LIBELLE")
	private String libelle;

	public Espace() {
	}
	
	public Espace(String string) {
		this.libelle = string;
	}

	public int getIdEspace() {
		return id;
	}

	public void setIdEspace(int id) {
		this.id = id;
	}

	public String getLibelle() {
		return libelle;
	}

	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}
	
	
}
