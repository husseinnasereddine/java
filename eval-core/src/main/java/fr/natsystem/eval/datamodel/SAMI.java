package fr.natsystem.eval.datamodel;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="SAMI")
public class SAMI implements Serializable{

	private static final long serialVersionUID = -6216169949722375369L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="SAMI_ID")
	private int id;


	@Column(name="SAMI_CODE")
	private String code;
	
	@Column(name="SAMI_VALEUR")
	private int valeur;
	
	@Column(name="SAMI_LIBELLE")
	private String libelle;
	
	@Column(name="SAMI_SEUIL")
	private int seuil;
	
	@Column(name="SAMI_DESCRIPTION")
	private String descriptionSeuil;

	@Column(name="SAMI_COULEUR")
	private String color;
	
	public SAMI() {
	}
	
	public SAMI(String code, int valeur, String libelle, int seuil,
			String descriptionSeuil) {
		super();
		this.code = code;
		this.valeur = valeur;
		this.libelle = libelle;
		this.seuil = seuil;
		this.descriptionSeuil = descriptionSeuil;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public int getValeur() {
		return valeur;
	}

	public void setValeur(int valeur) {
		this.valeur = valeur;
	}

	public String getLibelle() {
		return libelle;
	}

	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}

	public int getSeuil() {
		return seuil;
	}

	public void setSeuil(int seuil) {
		this.seuil = seuil;
	}

	public String getDescriptionSeuil() {
		return descriptionSeuil;
	}

	public void setDescriptionSeuil(String descriptionSeuil) {
		this.descriptionSeuil = descriptionSeuil;
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

}
