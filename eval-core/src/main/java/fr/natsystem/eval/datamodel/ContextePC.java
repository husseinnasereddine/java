package fr.natsystem.eval.datamodel;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="CONTEXTE_PC")
public class ContextePC implements Serializable{
	
	private static final long serialVersionUID = -2390625477324180882L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="CONTEXTE_PC_ID")
	private int id;
	
	@ManyToOne
	@JoinColumn(name="CONTEXTE_PC_COEFF")
	private CoefficientContextePC coefficientContextePCRef;

	@ManyToOne
	@JoinColumn(name="CONTEXTE_PC_POINT_CLE")
	private PointCle pointCleRef;
	
	@ManyToOne
	@JoinColumn(name="CONTEXTE_PC_AGENT")
	private Agent DPXRef;
	
	
	public ContextePC() {
	}	
	
	public ContextePC(CoefficientContextePC coeffContextePC, PointCle pointCleRef, Agent DPXRef) {
		super();
		this.coefficientContextePCRef = coeffContextePC;
		this.pointCleRef = pointCleRef;
		this.DPXRef = DPXRef;
	}


	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public CoefficientContextePC getCoefficientContextePCRef() {
		return coefficientContextePCRef;
	}

	public void setCoefficientContextePCRef(
			CoefficientContextePC coefficientContextePCRef) {
		this.coefficientContextePCRef = coefficientContextePCRef;
	}

	public PointCle getPointCleRef() {
		return pointCleRef;
	}

	public void setPointCleRef(PointCle pointCleRef) {
		this.pointCleRef = pointCleRef;
	}

	public Agent getDPXRef() {
		return DPXRef;
	}

	public void setDPXRef(Agent dPXRef) {
		DPXRef = dPXRef;
	}
	
	

}

