package fr.natsystem.eval.dao;

import java.util.List;

import fr.natsystem.eval.datamodel.Domaine;
import fr.natsystem.eval.datamodel.Espace;
import fr.natsystem.eval.datamodel.PointCle;
import fr.natsystem.eval.datamodel.Theme;



public interface IPointsClesDAO {
	
	public List<PointCle> getAllPointsCles();
	public List<PointCle> getAllPointsClesOrderedByTheme();
	public List<PointCle> getPointsClesParTheme(Theme theme);
	public List<PointCle> getPointsClesParDomaine(Domaine domaine);
	public List<PointCle> getPointsClesParEspace(Espace espace);

}
