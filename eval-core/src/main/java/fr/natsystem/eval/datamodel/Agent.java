package fr.natsystem.eval.datamodel;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name="AGENT")
public class Agent implements Serializable{

	private static final long serialVersionUID = 336508482916556254L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="AGENT_ID")
	private int id;
	
	@Column(name="AGENT_NUMERO_CP")
	private String numCP;
	
	@Column(name="AGENT_NOM")
	private String nom;
	
	@Column(name="AGENT_PRENOM")
	private String prenom;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="AGENT_REF")
	private Agent agentRef;
	
	@ManyToOne
	@JoinColumn(name="AGENT_UNITE")
	private BusinessUnit unite;
	
	@ManyToOne
	@JoinColumn(name="AGENT_CONTEXTE_AGENT")
	private CoefficientContexteAgent coeffContexteAgentRef;
	
	@Transient
	private String formattedRoles;

	public Agent getAgentRef() {
		return agentRef;
	}

	public void setAgentRef(Agent agentRef) {
		this.agentRef = agentRef;
	}

	public BusinessUnit getUnite() {
		return unite;
	}

	public void setUnite(BusinessUnit unite) {
		this.unite = unite;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public CoefficientContexteAgent getCoeffContexteAgentRef() {
		return coeffContexteAgentRef;
	}

	public void setCoeffContexteAgentRef(
			CoefficientContexteAgent coeffContexteAgentRef) {
		this.coeffContexteAgentRef = coeffContexteAgentRef;
	}


	public String getNumCP() {
		return numCP;
	}

	public void setNumCP(String numCP) {
		this.numCP = numCP;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	

	public String getFormattedRoles() {
		return formattedRoles;
	}

	public void setFormattedRoles(String formattedRoles) {
		this.formattedRoles = formattedRoles;
	}

	public Agent(){
		
	}

	public Agent(String prenom, String nom, String numCP) {
		this.numCP = numCP;
		this.nom = nom;
		this.prenom = prenom;
	}
	
	public Agent(String prenom, String nom, String numCP, Agent agentRef, BusinessUnit unite, CoefficientContexteAgent coeffContexteAgentRef) {
		this.numCP = numCP;
		this.nom = nom;
		this.prenom = prenom;
		this.agentRef = agentRef;
		this.unite = unite;
		this.coeffContexteAgentRef = coeffContexteAgentRef;
	}
	
	
	public Agent(Agent agent) {
		super();
		this.id = agent.getId();
		this.numCP = agent.getNumCP();
		this.nom = agent.getNom();
		this.prenom = agent.getPrenom();
		if (agent.getAgentRef() != null) {
			Agent agentRef = new Agent(agent.getAgentRef());
			this.agentRef = agentRef;
		}
		if (agent.getUnite() != null) {
			BusinessUnit unite = new BusinessUnit(agent.getUnite());
			this.unite = unite;
		}
		this.coeffContexteAgentRef = agent.getCoeffContexteAgentRef();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((coeffContexteAgentRef == null) ? 0 : coeffContexteAgentRef
						.hashCode());
		result = prime * result + id;
		result = prime * result + ((nom == null) ? 0 : nom.hashCode());
		result = prime * result + ((numCP == null) ? 0 : numCP.hashCode());
		result = prime * result + ((prenom == null) ? 0 : prenom.hashCode());
		result = prime * result + ((unite == null) ? 0 : unite.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Agent other = (Agent) obj;
		if (coeffContexteAgentRef == null) {
			if (other.coeffContexteAgentRef != null)
				return false;
		} else if (!coeffContexteAgentRef.equals(other.coeffContexteAgentRef))
			return false;
		if (id != other.id)
			return false;
		if (nom == null) {
			if (other.nom != null)
				return false;
		} else if (!nom.equals(other.nom))
			return false;
		if (numCP == null) {
			if (other.numCP != null)
				return false;
		} else if (!numCP.equals(other.numCP))
			return false;
		if (prenom == null) {
			if (other.prenom != null)
				return false;
		} else if (!prenom.equals(other.prenom))
			return false;
		if (unite == null) {
			if (other.unite != null)
				return false;
		} else if (!unite.equals(other.unite))
			return false;
		return true;
	}

	public String getPrenomNom() {
		return prenom + "\n" + nom;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Agent [id=" + id + ", numCP=" + numCP + ", nom=" + nom + ", prenom=" + prenom + ", agentRef=" + agentRef
				+ ", unite=" + unite + ", coeffContexteAgentRef=" + coeffContexteAgentRef + ", formattedRoles="
				+ formattedRoles + "]";
	}
	
	
}
