package fr.natsystem.eval.dao.impl;

import java.util.List;

import javax.inject.Inject;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Component;

import fr.natsystem.eval.datamodel.Agent;
import fr.natsystem.eval.datamodel.Cycle;
import fr.natsystem.eval.datamodel.EvaluationPCAgent;
import fr.natsystem.eval.datamodel.PointCle;
import fr.natsystem.eval.datamodel.Theme;

@Component
public class EvaluationPCAgentDAO{
	
	@Inject
	private CycleDAO cDAO;
	
	@Inject 
	private SessionFactory sf;
	
	public EvaluationPCAgent getEvaluationParPCetAgent(PointCle pointCle, Agent agent) {
		// TODO complete
		return null;
	}
	
	public List<EvaluationPCAgent> getEvaluationsEffectueesParAgent(Agent agent) {
		// TODO complete
		return null;
	}

	public List<EvaluationPCAgent> getEvaluationsEffectueesParPointCle(PointCle pointCle) {
		// TODO complete
		return null;
	}

	public void savePrimitiveMsNoTx(EvaluationPCAgent entity) {
		// TODO complete
		
	}
	

	public List<EvaluationPCAgent> getEvaluationsPourAgentsEtPointsCles(List<Agent> listeAgents, List<PointCle> listePointsCles) {
		//TODO complete
		//given as an example
		
		Cycle cycle = cDAO.getLastCycle();
		String stringQuery = "select eval from EvaluationPCAgent eval where eval.cycleRef = :cycle and eval.flagActif = true ";
		if (listeAgents == null || listeAgents.size() == 0 || listePointsCles == null || listePointsCles.size() == 0) {
			return null;
		}
		int sizeAgents = listeAgents.size();
		int sizePC = listePointsCles.size();
		for (int i=0 ; i < sizeAgents ; i++) {
			if (i == 0 ) {
				stringQuery += "and ( ";
			}
			if (i == sizeAgents-1){
				stringQuery += "eval.agentRef = :agent" + i + " ) ";
			} else {
				stringQuery += "eval.agentRef = :agent" + i + " or ";
			}
		}
		for (int j=0 ; j < sizePC ; j++) {
			if (j == 0) {
				stringQuery += "and ( ";
			}
			if (j == sizePC-1) {
				stringQuery += "eval.pointcleRef = :pointcle" + j + " ) "; 
			} else {
				stringQuery += "eval.pointcleRef = :pointcle" + j + " or ";
			}
		}
		Session session = sf.openSession();
		Query query = session.createQuery(stringQuery);
		query.setInteger("cycle", cycle.getId());
		for (int i=0 ; i < sizeAgents ; i++) {
			query.setInteger("agent" + i, listeAgents.get(i).getId());
		}
		for (int j=0 ; j < sizePC ; j++) {
			query.setInteger("pointcle" + j, listePointsCles.get(j).getId());
		}
		List list = query.list();
		return list;
	}
	
	/**
	 * Retourne la liste des évaluations en bases pour une liste d'agents et de points clés données.
	 * Utilisée lors des méthodes de sauvegarde des affections de PC.
	 * @param listeAgents
	 * @param listePointsCles
	 * @return
	 */
	public List<EvaluationPCAgent> getAllEvaluationsPourAgentsEtPointsCles(List<Agent> listeAgents, List<PointCle> listePointsCles) {
		// TODO complete
		return null;
	}
	
	/**
	 * Retourne la liste des évaluations en bases pour une liste d'agents et de points clés données.
	 * Utilisée lors des méthodes de sauvegarde des affections par thème.
	 * Cette liste est ordonnée afin d'améliorer le temps de sauvegarde
	 * @param listeAgents
	 * @param listePointsCles
	 * @return
	 */
	public List<EvaluationPCAgent> getAllOrderedEvaluationsPourAgentsEtPointsCles(List<Agent> listeAgents, List<Theme> listeThemes) {
		// TODO complete
		return null;
	}
	
	public List<EvaluationPCAgent> getEvaluationsPourAgents(List<Agent> listeAgents) {
		// TODO complete
		return null;
	}
	
	/**
	 * Retourne les évaluations pour les agents passés en paramètre et ordonnées
	 * par thème, par agent, et par ordre.
	 * @param listeAgents
	 * @return
	 */
	public List<EvaluationPCAgent> getOrderedEvaluationsPourAgents(List<Agent> listeAgents) {
		// TODO complete
		return null;
	}
	
	/**
	 * Récupération des évaluations PC du dernier cycle concernant un ensemble d'agents.
	 * @param listeAgents
	 * @return
	 */
	public List<EvaluationPCAgent> getAllEvaluationsPourAgents(List<Agent> listeAgents) {
		// TODO complete
		return null;
	}
	
	public List<EvaluationPCAgent> getEvaluationsPourAgentsEtPointsCles200EtPlus(List<Agent> listeAgents, List<PointCle> listePointsCles) {
		// TODO complete
		return null;
	}

}
