/**
 * 
 */
package fr.natsystem.eval.dao;

import java.util.List;

import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import fr.natsystem.eval.datamodel.Agent;
import fr.natsystem.eval.datamodel.BusinessUnit;

@Repository
public interface IAgentRepository extends JpaRepository<Agent, Long>{

	
	
	/**
	 * Returns the list of all agents attached to the specified agent id
	 * @param id the manager id
	 * @param sort the sorting direction
	 * @return a list of agent attached to the provided agent
	 */
	@Query("from Agent as agent where agent.agentRef = :agent")
	List<Agent> findAgentsByAgentRef(@Param("agent") final Agent agent, Sort sort);

	/**
	 * Returns the list of all agents attached to a BusinessUnit and with the specified Role name 
	 * @param unit the BU to get the agents from
	 * @param roleName the targeted agents role
	 * @return a list of agent
	 */
	List<Agent> findAgentsWithRoleAndBusinessUnit(@Param("unite") BusinessUnit unit, @Param("roleName") String roleName);
	
	/**
	 * Returns the list of all agents attached to a BusinessUnit and with the specified Role name 
	 * @param unit the BU to get the agents from
	 * @param roleName the targeted agents role
	 * @return a list of agent
	 */
	List<Agent> findAgentsByUnite(BusinessUnit unit);
	

	List<Agent> findAgentsByNumCP(String numCP);

	

}
