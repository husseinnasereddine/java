package fr.natsystem.eval.test;

import java.util.Collection;

import javax.inject.Inject;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import fr.natsystem.eval.dao.impl.BusinessUnitDS;
import fr.natsystem.eval.dao.impl.PointCleDAO;
import fr.natsystem.eval.datamodel.Theme;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"/applicationContext.xml"})
public class TestPointCleDAO {
	
	@Inject
	PointCleDAO dao;
	
	
	@Inject
	BusinessUnitDS bDAO;
	

	
	@Test
	public void testPointsClesParTheme(){
		
		Theme theme = new Theme();
		theme.setId(1);
		Collection<?> coll =  dao.getPointsClesParTheme(theme);
		System.out.println(coll);
	}
}
