package fr.natsystem.eval.test;

import javax.inject.Inject;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import fr.natsystem.eval.dao.impl.GraviteDAO;
import fr.natsystem.eval.datamodel.Domaine;
import fr.natsystem.eval.datamodel.Espace;
import fr.natsystem.eval.datamodel.Gravite;
import fr.natsystem.eval.datamodel.PointCle;
import fr.natsystem.eval.datamodel.Theme;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"/applicationContext.xml"})
public class TestCreationPointCle {
	
	@Inject
	GraviteDAO gDAO;
	
	@Inject 
	SessionFactory factory;
	
	
	@Test
	public void TestCreationPointCle(){
		Espace espace = new Espace("Défaut");
		Domaine domaine = new Domaine("Généralités", espace);
		Theme theme = new Theme("Formations", domaine);
		
		Gravite gr1 = gDAO.getByValeur(1);
		Gravite gr2 = gDAO.getByValeur(2);
		Gravite gr3 = gDAO.getByValeur(3);
		Gravite gr4 = gDAO.getByValeur(4);
		Gravite gr5 = gDAO.getByValeur(5);
		Gravite gr6 = gDAO.getByValeur(6);
		
//		PointCle pointcle1 = new PointCle("Formation Risque Electrique", theme, gr1);
//		PointCle pointcle2 = new PointCle("Formation Risque Ferroviaire ", theme, gr2);
//		PointCle pointcle3 = new PointCle("Formation Risque PRAP", theme, gr3);
		PointCle pointcle4 = new PointCle("Formation Risque Chimique", theme, gr6, 1);
		PointCle pointcle5 = new PointCle("Formation Risque Rayonnement", theme, gr4, 2);
		PointCle pointcle6 = new PointCle("Formation Travaux hors Sol", theme, gr1, 3);
		PointCle pointcle7 = new PointCle("Formation Sûreté des MD", theme, gr2, 4);
		PointCle pointcle8 = new PointCle("Formations Risques Environnementaux ", theme, gr3, 5);
		
		Theme theme2 = new Theme("Comportement En Situation", domaine);
		
//		PointCle pointcle9 = new PointCle("Immixtions", theme2, gr1);
//		PointCle pointcle10 = new PointCle("Mise en oeuvre de l'autocontrôle", theme2, gr5);
		PointCle pointcle11 = new PointCle("Tenue et environnement du poste de travail", theme2, gr4, 6);
		PointCle pointcle12 = new PointCle("Montée et descente d'EM ou matériel roulant ", theme2, gr4, 7);
		PointCle pointcle13 = new PointCle("Déplacements et stationnements", theme2, gr3, 8);
		PointCle pointcle14 = new PointCle("Intervention sur le matériel roulant", theme2, gr4, 9);
		
		Session session = this.factory.openSession();
		Transaction tx = session.beginTransaction();
        
		session.saveOrUpdate(espace);
		session.saveOrUpdate(domaine);
		session.saveOrUpdate(theme);
		session.saveOrUpdate(theme2);
//		session.saveOrUpdate(pointcle1);
//		session.saveOrUpdate(pointcle2);
//		session.saveOrUpdate(pointcle3);
		session.saveOrUpdate(pointcle4);
		session.saveOrUpdate(pointcle5);
		session.saveOrUpdate(pointcle6);
		session.saveOrUpdate(pointcle7);
		session.saveOrUpdate(pointcle8);
//		session.saveOrUpdate(pointcle9);
//		session.saveOrUpdate(pointcle10);
		session.saveOrUpdate(pointcle11);
		session.saveOrUpdate(pointcle12);
		session.saveOrUpdate(pointcle13);
		session.saveOrUpdate(pointcle14);
		
		for (int i=10 ; i<200 ; i++) {
			PointCle pointcle = new PointCle("pointCle"+i, theme, gr4, i);
			session.saveOrUpdate(pointcle);
		}
		tx.commit();
		
		
	}

}
